#!/usr/bin/env bash

# Setup dev tools

# Note: you can also run without `sudo` and move the binary yourself
curl -sLS https://get.arkade.dev | sudo sh

# last i checked it only accepts one at a time.
arkade get doctl
arkade get terraform
arkade get tkn
arkade get docker-compose
arkade get yq
arkade get buildx
arkade get faas-cli
arkade get gh
arkade get helm
arkade get istioctl
arkade get jq
arkade get kail
arkade get kind
arkade get kubectl
arkade get kubectx
arkade get kubens
arkade get kubeseal
arkade get mc
arkade get pack
arkade get packer
arkade get kubeseal


# Install some common node packages
npm install --global npm yarn pnpm nx ts-node @teambit/bvm prettier pm2 prisma

## Install Bit
bvm install
bit init --harmony
bit install 

# Add update the PATH variable
export PATH=$PATH:$HOME/.arkade/bin:$HOME/bin
echo 'export PATH=$PATH:$HOME/.arkade/bin:$HOME/bin' >> ~/.bashrc



# Or install with:
# sudo mv /home/gitpod/.arkade/bin/doctl /usr/local/bin/